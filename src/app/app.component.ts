import { Component, OnInit } from '@angular/core';
import { AngularFirestore, 
         AngularFirestoreDocument, 
         AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { DatabaseService, Note } from './database.service';

export interface Note { name: string; }


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  notes: Observable<Note[]>;
  username: string;
  selectedId: string;
  title = 'blanknotes';

  constructor(private dbs: DatabaseService){
    this.username = '';
  }

  ngOnInit() {
  }

  async setUsername(username: string) {
    this.username = username;
    // Set the firestore username.
    this.dbs.setUsername(this.username);
    this.notes = this.dbs.getNotesCollection();
  }


  postNote(value: string) {
    this.dbs.addNote(value);
  }


  selectNote(noteId: string) {
    this.selectedId = noteId;
  }


  updateNote(noteId: string, text: string) {
    this.dbs.updateNote(noteId, text);
  }

  
  deleteNote(noteId: string){
    this.dbs.deleteNote(noteId);
  }
}
