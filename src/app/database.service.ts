import { Injectable } from '@angular/core';
import { AngularFirestore,
         AngularFirestoreDocument,
         AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface User { username: string, }
export interface Note { 
  id?: string,
  title?: string, 
  text: string,
}


@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  username: string;
  notesCollection: AngularFirestoreCollection<Note>;


  constructor(private afs: AngularFirestore) { 
  }


  setUsername(username: string) : void {
    // Make sure you set username before performing any user based tasks.
    // All tasks will only be done for one specific user.
    this.username = username;
    this.notesCollection = this.afs.collection<Note>(
      'users/' + this.username + '/notes',
      ref => ref.orderBy('text', 'asc'));
  }


  getNotesCollection() : Observable<Note[]>{
    // Retrieve the notes collection for the user, which can be observed
    // with .valueChanges().
    if(!this.username) {
      throw new Error('Make sure username is set.');
    }
    return this.notesCollection.snapshotChanges().
      pipe(
        map(changes => {
          return changes.map(a => {
            const data = a.payload.doc.data() as Note;
            data.id = a.payload.doc.id;
            return data;
          });
        })
      );
  }


  addNote(text: string) : void{
    // Add a single note to the user.
    if(!this.username) {
      throw new Error('Make sure username is set.');
    }
    this.notesCollection.add({ text: text } as Note);
  }


  updateNote(noteId: string, text: string, title?: string) {
    this.notesCollection.doc(noteId).update({id: noteId, text: text} as Note);
  }


  deleteNote(noteId: string){
    this.notesCollection.doc(noteId).delete();
  }


  // userExists(username: string) : Promise<boolean> {
  //   // Dunno if this is necessary, but it's here for now.
  //   return this.afs.collection('users')
  //     .doc(username)
  //     .get()
  //     .toPromise()
  //     .then(doc => {
  //       return doc.exists;
  //     });
  // }


  // noteExists(noteId: string) : Promise<boolean> {
  //   return this.afs.collection('users/' + this.username + '/notes/')
  //     .doc(noteId)
  //     .get()
  //     .toPromise()
  //     .then(doc => {
  //       return doc.exists;
  //     })
  // }
}
